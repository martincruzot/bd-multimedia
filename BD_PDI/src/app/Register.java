package app;


import java.util.Arrays;


public class Register {
    private int index = 0;
    private int next = -1;
    private double[] patterns = new double[9];  
    String pathFoto;
    
    public Register() {
    }
    
    public Register(double[] patterns, String pathFoto) {
        this.patterns = patterns;
        this.pathFoto = pathFoto;
    }

    public double[] getPatterns() {
        return this.patterns;
    }

    public void setPatterns(double[] patterns) {
        this.patterns = patterns;
    }

    public double getPattern(int index) {
        return patterns[index];
    }

    public void setPattern(int index, double value) {
        this.patterns[index] = value;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getNext() {
        return next;
    }

    public void setNext(int next) {
        this.next = next;
    }

    public String getPathFoto() {
        return pathFoto;
    }

    public void setPathFoto(String pathFoto) {
        this.pathFoto = pathFoto;
    }

    public int getBytesSize() {
        return ( patterns.length * ABBFile.SIZE_DOUBLE) + (2 * ABBFile.SIZE_INT) ;
    }
    
    public int compare(Register reg) {
        
        for (int i = 0; i < 9; i++) {
            if (patterns[i] > reg.getPattern(i)) {
                return 1;
            }
            else if (patterns[i] < reg.getPattern(i)) {
                return -1; 
            }
        }
        return 0;
    }
    
    public void show(String message) {
        System.out.print(message);
        System.out.print(this.index + "\t" + this.next + "\t");
        System.out.println(Arrays.toString(this.patterns));
    }
   
}
