package app;


import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;
import java.awt.geom.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Imagen extends Component {

    BufferedImage img;
    
    double[][] matrizImg;	    //matrizImg de la imagen en escala de grises
    double[][] matrizImg_R;         //matrizImg de la imagen en R
    double[][] matrizImg_G;         //matriz de la imagen en G
    double[][] matrizImg_B;         //matriz de la imagen en B
    int columnas;           
    int filas;              

    public Imagen(String nombreImagen) {
  
        try {
            img = ImageIO.read(new File(nombreImagen));
            convertirImagenAMatriz();
        } catch (IOException e) {
        }
    }

    public Imagen(double[][] matriz) {
        convertirMatrizAImagen(matriz);
    }

    public double[] generarPatrones()  {
        
        double[] patrones = new double[9];
        double TF = filas / 3;  // tercia filas
        double TC = columnas /3;  // tercia columnas
        
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                
                if ( i >= 0   && j >= 0  && i <= TF  && j <= TC ) 
                {
                    patrones[0] += matrizImg[i][j];
                } 
                else if ( i >= 0   && j >= TC  && i <= TF  && j <= 2*TC ) 
                {
                    patrones[1] += matrizImg[i][j];
                } 
                else if ( i >= 0   && j >= 2*TC  && i <= TF  && j <= 3*TC ) 
                {
                    patrones[2] += matrizImg[i][j];
                }
                else if ( i >= TF   && j >= 0  && i <= 2*TF && j <= TC )  
                {
                    patrones[3] += matrizImg[i][j];
                }
                else if ( i >= TF   && j >= TC  && i <= 2*TF && j <= 2*TC )  
                {
                    patrones[4] += matrizImg[i][j];
                }
                else if ( i >= TF   && j >= 2*TC  && i <= 2*TF && j <= 3*TC )  
                {
                    patrones[5] += matrizImg[i][j];
                }
                else if ( i >= 2*TF && j >= 0  && i <= 3*TF && j <= TC ) 
                {
                    patrones[6] += matrizImg[i][j];
                }
                else if ( i >= 2*TF && j >= TC  && i <= 3*TF && j <= 2*TC ) 
                {
                    patrones[7] += matrizImg[i][j];
                }
                else if ( i >= 2*TF && j >= 2*TC  && i <= 3*TF && j <= 3*TC ) 
                {
                    patrones[8] += matrizImg[i][j];
                }
            }
        }
        double n = TF * TC;
        for (int i = 0; i < 9; i++) {
            patrones[i] = patrones[i] /n ;
        }
        
        
        return patrones;

    }

    public void convertirImagenAMatriz() {
        filas = img.getHeight();
        columnas = img.getWidth();

        matrizImg = new double[filas][columnas];
        matrizImg_R = new double[filas][columnas];
        matrizImg_G = new double[filas][columnas];
        matrizImg_B = new double[filas][columnas];
        double r;
        double g;
        double b;

        WritableRaster raster = img.getRaster();
        int numBandas = raster.getNumBands();

        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                if (numBandas == 3) {
                    r = raster.getSampleDouble(j, i, 0);
                    g = raster.getSampleDouble(j, i, 1);
                    b = raster.getSampleDouble(j, i, 2);

                    matrizImg[i][j] = (r + g + b) / 3;
                    matrizImg_R[i][j] = r;
                    matrizImg_G[i][j] = g;
                    matrizImg_B[i][j] = b;
                }
                if (numBandas == 1) {
                    matrizImg[i][j] = raster.getSampleDouble(j, i, 0);
                    matrizImg_R[i][j] = 255;
                    matrizImg_G[i][j] = 255;
                    matrizImg_B[i][j] = 255;
                }
            }
        }
    }

    public void convertirMatrizAImagen(double[][] matriz) {
        int alto = matriz.length;
        int ancho = matriz[0].length;

        img = new BufferedImage(ancho, alto, BufferedImage.TYPE_BYTE_GRAY);
        WritableRaster wr = img.getRaster();

        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < ancho; j++) {
                wr.setSample(j, i, 0, matriz[i][j]);
            }
        }
        img.setData(wr);
    }

    public void guardarImagen(double[][] matriz, String path) {
        BufferedImage img = new BufferedImage(matriz[0].length, matriz.length, BufferedImage.TYPE_BYTE_GRAY);
        WritableRaster wr = img.getRaster();

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                wr.setSample(j, i, 0, matriz[i][j]);
            }
        }

        img.setData(wr);

        try {
            ImageIO.write(img, "JPG", new File(path + ".jpg"));
        } catch (IOException e) {
        }
    }

    public void guardarImagen(double[][] matriz_R, double[][] matriz_G, double[][] matriz_B, String path) {
        BufferedImage imgn = new BufferedImage(matriz_R[0].length, matriz_R.length, BufferedImage.TYPE_INT_RGB);
        WritableRaster wr = imgn.getRaster();

        for (int i = 0; i < matriz_R.length; i++) {
            for (int j = 0; j < matriz_R[0].length; j++) {
                wr.setSample(j, i, 0, matriz_R[i][j]);
                wr.setSample(j, i, 1, matriz_G[i][j]);
                wr.setSample(j, i, 2, matriz_B[i][j]);
                System.out.println(matriz_R[i][j]);
            }
        }

        img.setData(wr);
        try {
            ImageIO.write(img, "JPG", new File(path + ".jpg"));
        } catch (IOException e) {
        }
    }
    
    public void saveImage(String path) throws IOException {
        ImageIO.write(img, "JPG", new File(path + ".jpg"));
    }

    @Override
    public void paint(Graphics g) {
        int x = 0;
        int y = 800;  //valor estandar de y
        int p = 0;
        int q = 0;

        x = img.getWidth(null) * y / img.getHeight(null);
        y = img.getHeight(null) * y / img.getHeight(null);

        g.drawImage(img, 100, 100, null);
        g.drawImage(img, 0, 0, x, y, 0, 0, img.getWidth(null), img.getHeight(null), null);
        //Graphics2D g2 = (Graphics2D) g;           

    }

    @Override
    public Dimension getPreferredSize() {
        int x = 0;
        int y = 800;
        if (img == null) {
            return new Dimension(100, 100);
        } else {
            x = img.getWidth(null) * y / img.getHeight(null);
            y = img.getHeight(null) * y / img.getHeight(null);
            return new Dimension(x, y);
        }
    }

    /*Coloca una imagen en una ventana*/
    public static void ventana(double matriz[][], String nombre) {
        Imagen imagen = new Imagen(matriz);

        try {
            JFrame f = new JFrame(nombre);
            f.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                }
            }
            );
            f.add(imagen);
            f.pack();
            f.setVisible(true);
            f.repaint();
        } catch (Exception e) {
        }
    }

    public double[][] getMatrizImg() {

        return matrizImg;
    }

    public int getColumnas() {
        return columnas;
    }

    public int getFilas() {
        return filas;
    }

    public BufferedImage getImg() {
        return img;
    }

    public double[][] getMatrizImg_B() {
        return matrizImg_B;
    }

    public double[][] getMatrizImg_G() {
        return matrizImg_G;
    }

    public double[][] getMatrizImg_R() {
        return matrizImg_R;
    }

    public void setColumnas(int columnas) {
        this.columnas = columnas;
    }

    public void setFilas(int filas) {
        this.filas = filas;
    }

    public void setMatrizImg(double[][] matrizImg) {
        this.matrizImg = matrizImg;
    }

    public void setImg(BufferedImage img) {
        this.img = img;
    }

    public void setMatrizImg_B(double[][] matrizImg_B) {
        this.matrizImg_B = matrizImg_B;
    }

    public void setMatrizImg_G(double[][] matrizImg_G) {
        this.matrizImg_G = matrizImg_G;
    }

    public void setMatrizImg_R(double[][] matrizImg_R) {
        this.matrizImg_R = matrizImg_R;
    }

}
