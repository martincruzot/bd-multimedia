package app;


public class Algoritmos
{
    public static double [][] operadorUmbral(Imagen img, int umbral)
    {
        double [][]I;	//matriz de la imagen
        int m;          //m filas
        int n;          //n columnas

        I=img.getMatrizImg();
        m=img.getFilas();
        n=img.getColumnas();

        for(int i=0;i<m;i++)
        {
            for(int j=0;j<n;j++)
            {
                if(I[i][j]<umbral)
                    I[i][j]=0;
                else
                    I[i][j]=255;
            }
        }
        return I;
    }

    public static double [][] operadorInverso(Imagen img)
    {
        double [][]I;	//matriz de la imagen
        int m;          //m filas
        int n;          //n columnas

        I=img.getMatrizImg();
        m=img.getFilas();
        n=img.getColumnas();

        for(int i=0;i<m;i++)
        {
            for(int j=0;j<n;j++)
            {
                I[i][j]=255-I[i][j];
            }
        }
        return I;
    }

}
