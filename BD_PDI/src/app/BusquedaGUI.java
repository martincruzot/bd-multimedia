package app;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Vector;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class BusquedaGUI extends javax.swing.JFrame {

    private Imagen imagen;
    private Vector<Register> vectResultados;

    public BusquedaGUI() {
        initComponents();
        lblSimilitud.setText(new Integer(sliderSimilitud.getValue()).toString());
        lblError.setText(new Integer(sliderError.getValue()).toString());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblImagenSelect = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblListaPatrones = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        lblImagenOriginal = new javax.swing.JLabel();
        btnCargarImagen = new javax.swing.JButton();
        sliderError = new javax.swing.JSlider();
        sliderSimilitud = new javax.swing.JSlider();
        lblError = new javax.swing.JLabel();
        lblSimilitud = new javax.swing.JLabel();
        btnBuscar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Resultados", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 13), new java.awt.Color(51, 102, 255))); // NOI18N

        lblImagenSelect.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Imagen resultado", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(51, 102, 255))); // NOI18N

        tblListaPatrones.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "index", "[1]", "[2]", "[3]", "[4]", "[5]", "[6]", "[7]", "[8]", "[9]"
            }
        ));
        tblListaPatrones.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblListaPatronesMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblListaPatrones);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 592, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(lblImagenSelect, javax.swing.GroupLayout.PREFERRED_SIZE, 324, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(145, 145, 145))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblImagenSelect, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(61, 61, 61))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Parametros de búsqueda", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 13), new java.awt.Color(0, 102, 255))); // NOI18N
        jPanel2.setToolTipText("");

        lblImagenOriginal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Original", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(0, 102, 255))); // NOI18N

        btnCargarImagen.setText("Cargar imagen");
        btnCargarImagen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargarImagenActionPerformed(evt);
            }
        });

        sliderError.setValue(10);
        sliderError.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Porcentaje de error", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(0, 102, 255))); // NOI18N
        sliderError.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                sliderErrorMouseDragged(evt);
            }
        });
        sliderError.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                sliderErrorMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                sliderErrorMouseReleased(evt);
            }
        });

        sliderSimilitud.setValue(2);
        sliderSimilitud.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Porcentaje de similitud", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(0, 102, 255))); // NOI18N
        sliderSimilitud.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                sliderSimilitudMouseDragged(evt);
            }
        });
        sliderSimilitud.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                sliderSimilitudMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                sliderSimilitudMouseReleased(evt);
            }
        });

        lblError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblError.setText("0");
        lblError.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblSimilitud.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSimilitud.setText("0");
        lblSimilitud.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(sliderSimilitud, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblSimilitud, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(sliderError, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(6, 6, 6))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblImagenOriginal, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(btnCargarImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sliderError, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(lblError)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(sliderSimilitud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(lblSimilitud)))
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCargarImagen)
                    .addComponent(btnBuscar))
                .addGap(7, 7, 7)
                .addComponent(lblImagenOriginal, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCargarImagenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarImagenActionPerformed

        JFileChooser chooser = new JFileChooser();
        int valorRetorno = chooser.showOpenDialog(null);

        if (valorRetorno == JFileChooser.APPROVE_OPTION) {
            File archivoElegido = chooser.getSelectedFile();
            String ruta = archivoElegido.getPath();

            // Poner vista previa de la imagen
            ImageIcon img = new ImageIcon(ruta);
            Icon imagenEscalada = new ImageIcon(img.getImage().getScaledInstance(lblImagenOriginal.getWidth(), lblImagenOriginal.getHeight(), Image.SCALE_DEFAULT));
            lblImagenOriginal.setIcon(imagenEscalada);
            this.repaint();

            // Cargar la imagen en buffer
            this.imagen = new Imagen(ruta);
        }
    }//GEN-LAST:event_btnCargarImagenActionPerformed

    private void sliderSimilitudMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliderSimilitudMouseDragged
        lblSimilitud.setText(new Integer(sliderSimilitud.getValue()).toString());
    }//GEN-LAST:event_sliderSimilitudMouseDragged

    private void sliderErrorMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliderErrorMouseDragged
        lblError.setText(new Integer(sliderError.getValue()).toString());
    }//GEN-LAST:event_sliderErrorMouseDragged

    private void sliderErrorMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliderErrorMousePressed
        lblError.setText(new Integer(sliderError.getValue()).toString());
    }//GEN-LAST:event_sliderErrorMousePressed

    private void sliderErrorMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliderErrorMouseReleased
        lblError.setText(new Integer(sliderError.getValue()).toString());
    }//GEN-LAST:event_sliderErrorMouseReleased

    private void sliderSimilitudMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliderSimilitudMousePressed
        lblSimilitud.setText(new Integer(sliderSimilitud.getValue()).toString());
    }//GEN-LAST:event_sliderSimilitudMousePressed

    private void sliderSimilitudMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliderSimilitudMouseReleased
        lblSimilitud.setText(new Integer(sliderSimilitud.getValue()).toString());
    }//GEN-LAST:event_sliderSimilitudMouseReleased

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed

        if (imagen != null) {
            try {
                ABBFile abb = new ABBFile(new File("patrones.txt"));
                Register x = new Register(imagen.generarPatrones(), "0");
                int error = sliderError.getValue();
                int similitud = sliderSimilitud.getValue();
                vectResultados = abb.getMathingRegisters(x, error, similitud);

                // Llenar valores en una tabla
                this.mostrarResultados(vectResultados);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Aun no cargado una imagen!");
        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void tblListaPatronesMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblListaPatronesMousePressed
    
        int row = tblListaPatrones.getSelectedRow();
        String path = PrincipalGUI.PATH_BASE + (new Integer(vectResultados.get(row).getIndex()).toString()) + ".jpg";
        System.out.println("Path: " + path);
        mostrarImagen(path);
  
    }//GEN-LAST:event_tblListaPatronesMousePressed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BusquedaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BusquedaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BusquedaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BusquedaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BusquedaGUI().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCargarImagen;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblError;
    private javax.swing.JLabel lblImagenOriginal;
    private javax.swing.JLabel lblImagenSelect;
    private javax.swing.JLabel lblSimilitud;
    private javax.swing.JSlider sliderError;
    private javax.swing.JSlider sliderSimilitud;
    private javax.swing.JTable tblListaPatrones;
    // End of variables declaration//GEN-END:variables

    private void mostrarResultados(Vector<Register> v) {
        String[] title = {"id", "[1]", "[2]", "[3]", "[4]", "[5]", "[6]", "[7]", "[8]", "[9]"};
        String[] rowTable = new String[100];
        DefaultTableModel tableModel = new DefaultTableModel(null, title);

        for (int i = 0; i < v.size(); i++) {
            rowTable[0] = (new Integer(v.get(i).getIndex()).toString());

            for (int j = 0; j < 9; j++) {
                rowTable[j + 1] = (new Float(v.get(i).getPattern(j)).toString());
            }
            tableModel.addRow(rowTable);
        }
        tblListaPatrones.setModel(tableModel);
    }

    private void mostrarImagen(String path) {
        ImageIcon img = new ImageIcon(path);
        Icon imagenEscalada = new ImageIcon(img.getImage().getScaledInstance(lblImagenSelect.getWidth(), lblImagenSelect.getHeight(), Image.SCALE_DEFAULT));
        lblImagenSelect.setIcon(imagenEscalada);
        this.repaint();
    }
}
