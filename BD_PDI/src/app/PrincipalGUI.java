package app;


import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class PrincipalGUI extends javax.swing.JFrame {

    javax.swing.JFileChooser jFileChooser1;
    Imagen imagen;
    String path;
    FileWriter fichero = null; 
    PrintWriter pw = null;
    
    private ABBFile abbfile;
    private File file = new File("patrones.txt");
    
    public static final String PATH_BASE = "imagesdb/";

    public PrincipalGUI() {
        
        jFileChooser1 = new javax.swing.JFileChooser();
        jFileChooser1.setFileSelectionMode(JFileChooser.FILES_ONLY);
        
        try {
            abbfile = new ABBFile(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
           
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        menuItemGuardarGris = new javax.swing.JMenu();
        menuItemAbrir = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        meunuItemGuardarRGB = new javax.swing.JMenuItem();
        menuItemUmbral = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuItemNegativo = new javax.swing.JMenuItem();
        menuBaseDatos = new javax.swing.JMenu();
        menuItemBusqueda = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Procesamiento Digital de Imagenes");

        menuItemGuardarGris.setText("Archivo");

        menuItemAbrir.setText("Abrir");
        menuItemAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemAbrirActionPerformed(evt);
            }
        });
        menuItemGuardarGris.add(menuItemAbrir);

        jMenuItem2.setText("Guardar Gris");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuItemGuardarGris.add(jMenuItem2);

        meunuItemGuardarRGB.setText("Guardar RGB");
        meunuItemGuardarRGB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                meunuItemGuardarRGBActionPerformed(evt);
            }
        });
        menuItemGuardarGris.add(meunuItemGuardarRGB);

        jMenuBar1.add(menuItemGuardarGris);

        menuItemUmbral.setText("Operaciones");

        jMenuItem3.setText("Umbral");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuItemUmbral.add(jMenuItem3);

        menuItemNegativo.setText("Negativo");
        menuItemNegativo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemNegativoActionPerformed(evt);
            }
        });
        menuItemUmbral.add(menuItemNegativo);

        jMenuBar1.add(menuItemUmbral);

        menuBaseDatos.setText("Base de Datos");

        menuItemBusqueda.setText("Búsqueda");
        menuItemBusqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemBusquedaActionPerformed(evt);
            }
        });
        menuBaseDatos.add(menuItemBusqueda);

        jMenuBar1.add(menuBaseDatos);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 507, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menuItemAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemAbrirActionPerformed
        
        int valorRetorno = jFileChooser1.showOpenDialog(null);
        
        if (valorRetorno == JFileChooser.APPROVE_OPTION) {
            File nombreImagen = jFileChooser1.getSelectedFile();
            path = nombreImagen.getAbsolutePath();
            imagen = new Imagen(path);

            try {
                JFrame f = new JFrame("Imagenes");
                f.setResizable(false);
                f.add(imagen);
                f.pack();
                f.setLocationRelativeTo(null);
                f.setVisible(true);
                
                // Guardar imagen en directorio
                Integer nr = abbfile.numRegisters() + 1;
                String namephoto = nr.toString();
                imagen.saveImage(PATH_BASE + namephoto);
                
                // Guardando patrones en fichero
                
                abbfile.writeRegister(new Register(imagen.generarPatrones(), namephoto));
                abbfile.showRegisters();
                
            } catch (Exception e) {
            }
        } else {
        }
    }//GEN-LAST:event_menuItemAbrirActionPerformed
    
   
    
    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        int valorRetorno = jFileChooser1.showSaveDialog(null);
        if (valorRetorno == JFileChooser.APPROVE_OPTION) {
            File archivoImagen = jFileChooser1.getSelectedFile();

            imagen.guardarImagen(imagen.getMatrizImg(), archivoImagen.getAbsolutePath());
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        imagen = new Imagen(path);
        int umbral = 150;
        Imagen.ventana(Algoritmos.operadorUmbral(imagen, umbral), "operador umbral");

    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void menuItemNegativoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemNegativoActionPerformed
        // TODO add your handling code here:

        imagen = new Imagen(path);
        Imagen.ventana(Algoritmos.operadorInverso(imagen), "operador umbral");

    }//GEN-LAST:event_menuItemNegativoActionPerformed

    private void meunuItemGuardarRGBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_meunuItemGuardarRGBActionPerformed
        // TODO add your handling code here:
        int valorRetorno = jFileChooser1.showSaveDialog(null);
        if (valorRetorno == JFileChooser.APPROVE_OPTION) {
            File archivoImagen = jFileChooser1.getSelectedFile();

            imagen.guardarImagen(imagen.getMatrizImg_R(), imagen.getMatrizImg_G(), imagen.getMatrizImg_B(), archivoImagen.getAbsolutePath());
        }

    }//GEN-LAST:event_meunuItemGuardarRGBActionPerformed

    private void menuItemBusquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemBusquedaActionPerformed
        System.out.println("Abriendo interfaz de busqueda!");
        new BusquedaGUI().setVisible(true);
    }//GEN-LAST:event_menuItemBusquedaActionPerformed
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
                new PrincipalGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenu menuBaseDatos;
    private javax.swing.JMenuItem menuItemAbrir;
    private javax.swing.JMenuItem menuItemBusqueda;
    private javax.swing.JMenu menuItemGuardarGris;
    private javax.swing.JMenuItem menuItemNegativo;
    private javax.swing.JMenu menuItemUmbral;
    private javax.swing.JMenuItem meunuItemGuardarRGB;
    // End of variables declaration//GEN-END:variables

}
